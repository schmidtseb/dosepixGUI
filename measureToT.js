/* -- Establish client and connect -- */
const zerorpc = require("zerorpc")
let client = new zerorpc.Client()

client.connect("tcp://127.0.0.1:4242")
client.on("error", function(error) {
    console.error("RPC client error:", error);
});

console.log(client)

/* MEASURE ToT */
// Plot
let plotHeight = 150;
let plotWidth = 400;

var yourVlSpec = {
  "$schema": "https://vega.github.io/schema/vega-lite/v3.0.0-rc10.json",
  "description": "ToT Measurement",
  "data": {"name": "table", "values": {"ToT": 0, "Counts": 0}},
  "mark": {"type": "bar", "binSpacing": 0},
  "width": plotWidth,
  "height": plotHeight,
  "actions": {
    "export": true,
    "source": true,
    "compiled": false,
    "editor": false
  },
  "encoding": {
    "x": {"field": "ToT", "type": "quantitative"},
    "y": {"field": "Counts", "type": "quantitative"}
  }
}

// Show empty plot at start
vegaEmbed("#ToTPlot", yourVlSpec);

// Hide alert
$("#ToTAlert").hide();

// Variables
let ToTStart = document.querySelector('#ToTStart');
let ToTStartButton = document.querySelector('#ToTStartButton');
let ToTPlotButton = document.querySelector('#ToTPlotButton');
let plotOkay = document.querySelector('#PlotOkay');
let stat = document.querySelector('#status');
let output = document.querySelector('#outputFileInput');
let frames = document.querySelector('#framesInput');
let slotCheck1 = document.querySelector('#slotCheckbox1');
let slotCheck2 = document.querySelector('#slotCheckbox2');
let slotCheck3 = document.querySelector('#slotCheckbox3');

let runFlag = false;

// Open Plot Settings modal
ToTPlotButton.addEventListener("click", () => {
  console.log('Open settings');
  $('#PlotModal').modal('show');
});

// Plot Settings confirmation
plotOkay.addEventListener("click", () => {
  let plotHeight = $('#plotHeight').value;
  let plotWidth = $('#plotWidth').value;
  $('#PlotModal').modal('hide');
});

// Open Measurement modal
ToTStartButton.addEventListener("click", () => {
  // Check if already running
  console.log(runFlag);
  if(runFlag) {
    client.invoke("stopProcess", (error, res, more) => {
      if(error) {
        console.error(error);
      }
    });
  } else {
    $('#ToTModal').modal('show');
  }
});

// Start measurement
ToTStart.addEventListener("click", () => {
  // Check if at least one slot is enabled
  if(!(slotCheck1.checked || slotCheck2.checked || slotCheck3.checked)) {
    $("#ToTAlert").show();
    return;
  }

  // Set to standard values if no input is present
  if(output.value.length == 0)
    output.value = "ToTMeasurement";
  if(frames.value.length == 0)
    frames.value = 1000;

  console.log(output.value);
  console.log(frames.value);
  $("#ToTAlert").hide();
  $('#ToTModal').modal('hide');

  // Start function
  runFlag = true;

  // Change button text and color
  $("#ToTStartButton").html("Stop Measurement");
  $("#ToTStartButton").removeClass('btn-primary').addClass('btn-danger');

  // Update plot
  vegaEmbed("#ToTPlot", yourVlSpec).then(function(result) {
    window.onresize = function (event) {
      result.view.signal('width', event.target.innerWidth - 200);
      result.view.run('enter');
    }

    console.log(result.view.getState());

    client.invoke("measureToT", output, frames, (error, res, more) => {
      if (!more) {
        console.log("Done.");
        // End of function
        runFlag = false;
        $("#ToTStartButton").html("Start Measurement");
        $("#ToTStartButton").removeClass('btn-danger').addClass('btn-primary');
      } else {
        if(error) {
          console.error(error);
        } else {
          let resJ = JSON.parse(res);
          let data = resJ.data;
          let status = resJ.status;
          stat.textContent = status + '%';

          document.getElementById('progress').style.width=status+'%';
          change = vega.changeset().insert(data).remove(vega.truthy);
          result.view.change("table", change).run();
        }
      }
    })
  });
})
