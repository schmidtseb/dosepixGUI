/* -- Establish client and connect -- */
const zerorpc = require("zerorpc")
let client = new zerorpc.Client()

client.connect("tcp://127.0.0.1:4242")
client.on("error", function(error) {
    console.error("RPC client error:", error);
});

client.invoke("echo", "server ready", (error, res) => {
  if(error || res !== 'server ready') {
    console.error(error)
  } else {
    console.log("server is ready")
  }
})

console.log(client)

/* -- Hide and show content -- */
var contents = document.querySelectorAll('.content');
console.log(contents);

function hide(element) {
  element.style.display = "none";
}

function hideAll() {
  for (var i = 0; i < contents.length; i++) {
    hide(contents[i]);
  }
}

function show(element) {
  element.style.display = "block";
}

document.getElementById('homeMenu').addEventListener('click', function () {
  hideAll();
  show(document.getElementById('home'));
}, false);

document.getElementById('testMenu').addEventListener('click', function () {
  hideAll();
  show(document.getElementById('test'));
}, false);

document.getElementById('ToTMenu').addEventListener('click', function () {
  hideAll();
  show(document.getElementById('measureToT'));
}, false);

/* -- Page Functions -- */
let DPXStart = document.querySelector('#DPXStart');
let result = document.querySelector('#result');

DPXStart.addEventListener("click", () => {
  client.invoke("connectDPX", (error, res) => {
    if(error) {
      console.error(error);
    } else {
      console.log('Successfully connected to DPX!')
      result.textContent = res;
    }
  })
})

let DPXOnFlag = false;

// Search for DPX every five seconds
window.setInterval(function(){
  client.invoke("findDPX", (error, res) => {
    if(error) {
      console.error(error);
    } else {
      if(res) {
        result.textContent = "Found DPX!";
        client.invoke("connectDPX", (error, res) => {
          if(error) {
            console.error(error);
          } else {
            result.textContent = "DPX connected!";
            DPXonFlag = true;
            window.clearInterval();
          }
        });
      } else {
        result.textContent = "DPX not found!";
      }
    }
  });
}, 5000);

DPXStart.dispatchEvent(new Event('input'));

let Test = document.querySelector('#Test');
let resTest = document.querySelector('#resTest');
let progress = document.querySelector('#progress');
let stat = document.querySelector('#status');

Test.addEventListener("click", () => {
  client.invoke("testPlot", (error, res, more) => {
    if (!more) {
      console.log("Done.");
    } else {
      if(error) {
        console.error(error);
      } else {
        // resTest.textContent = res;

        let resJ = JSON.parse(res);
        let data = resJ.data;
        // console.log(resJ);
        let status = resJ.status;
        stat.textContent = status + '%';
        // console.log(status);

        // progress.css("width", status+ '%').attr("aria-valuenow", status+ '%').text(status+ '%'); 
        document.getElementById('progress').style.width=status+'%';

        var yourVlSpec = {
          "$schema": "https://vega.github.io/schema/vega-lite/v2.json",
          "description": "A scatterplot showing horsepower and miles per gallons for various cars.",
          "data": {"values": data, "format": "json"},
          "mark": "point",
          "encoding": {
            "x": {"field": "x", "type": "quantitative"},
            "y": {"field": "y", "type": "quantitative"}
          }
        }
        vegaEmbed("#vis", yourVlSpec);
      }
    }
  })
})

Test.dispatchEvent(new Event('input'))

/*
let formula = document.querySelector('#formula')
let result = document.querySelector('#result')

formula.addEventListener('input', () => {
  client.invoke("calc", formula.value, (error, res) => {
    if(error) {
      console.error(error)
    } else {
      result.textContent = res
    }
  })
})
*/

// formula.dispatchEvent(new Event('input'))
